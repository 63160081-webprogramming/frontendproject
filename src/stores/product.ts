import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import http from "@/services/axios";
import productService from "@/services/product";
import { useLoadingStore } from "./loading";
import { useMassageStore } from "./massage";

export const useProductStore = defineStore("Product", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMassageStore();
  const products = ref<Product[]>([]);
  const dialog = ref(false);
  const editedProduct = ref<Product>({ name: "", price: 0 });
  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedProduct.value = { name: "", price: 0 };
    }
  });
  async function getProduct() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts();
      products.value = res.data;
    } catch (error) {
      console.log(error);
      messageStore.ShowError("ไม่สามารถดึง Product ได้");
    }
    loadingStore.isLoading = false;
  }
  async function saveProduct() {
    loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        const res = await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        const res = await productService.saveProduct(editedProduct.value);
      }

      dialog.value = false;
      //clearProduct();
      await getProduct(); // update ตัวมันเอง
    } catch (error) {
      console.log(error);
      messageStore.ShowError("ไม่สามารถบันทึก Product ได้");
    }
    loadingStore.isLoading = false;
  }
  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }
  async function deleteProduct(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await productService.deleteProduct(id);

      await getProduct(); // update ตัวมันเอง
    } catch (error) {
      console.log(error);
      messageStore.ShowError("ไม่สามารถลบ Product ได้");
    }
    loadingStore.isLoading = false;
  }
  return {
    products,
    getProduct,
    dialog,
    editedProduct,
    saveProduct,
    editProduct,
    deleteProduct,
  };
});
