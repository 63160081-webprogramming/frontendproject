import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useMassageStore = defineStore("Massage", () => {
  const isShow = ref(false);
  const massage = ref("");

  function ShowError(txt: string) {
    massage.value = txt;
    isShow.value = true;
  }

  return { isShow, massage, ShowError };
});
